defmodule Concat do
  def join(a, b, sep \\ " ") do
    a <> sep <> b
  end
end

IO.inspect Concat.join("Hello", "World", "_")
IO.inspect Concat.join("Hello", "World")
