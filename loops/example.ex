defmodule Main do
  def main do
    for x <- [1,2,3] do
      x + 1
    end

    for y <- 1..10 do
      y * 2
    end
  end

  def accumulate([], _acc, init) do
    init
  end

  def accumulate([head | tail], acc, init) do
    accumulate(tail, acc, acc.(init, head))
  end

  def sum(lst) do
    accumulate(lst, fn a, b -> a + b end, 0)
  end

  def factorial(lst) do
    accumulate(lst, fn a, b -> a * b end, 1)
  end
end


IO.inspect Main.main
IO.inspect Main.sum([1,2,3,4,5,6])
IO.inspect Main.factorial([1,2,3,4,5,6])
