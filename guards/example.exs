defmodule Math do
  def zero?(0), do: true
  # when condition is a guard
  def zero?(x) when is_number(x), do: false
  def zero?(_), do: {:error, "Cannot calculate for non-numbers."}
end

IO.puts Math.zero?(0)
IO.puts Math.zero?(5)
IO.inspect Math.zero?("1")
