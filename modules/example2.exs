defmodule Math do
  def add(a, b) do
    a + b
  end

  # PRIVATE
  defp do_add(a, b) do
    a + b
  end
end

IO.puts Math.do_add(1, 2)
