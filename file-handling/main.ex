defmodule FileHandling do
  def write_to_file(path, content) do
    case File.open path, [:write] do
      {:ok, file} -> IO.binwrite file, content
      {:error, _some_err} -> "could not write to file"
    end
  end

  def read_from_file(path) do
    case File.read path do
      {:ok, contents} -> contents
      {:error, _some_err} -> "could not read from file"
    end
  end
end

FileHandling.write_to_file("testfile.txt", "Hello World!")
IO.inspect FileHandling.read_from_file "testfile.txt"
