defmodule Main do
  def main do
    # This pattern matching will bind a to 1.
    %{:a => a} = %{:a => 1, 2 => :b}
    IO.puts a
    # This pattern matching will match, because the subset of keys
    # against which we match is in the map on the right hand side.
    %{} = %{:a => 1, 2 => :b}
    # This pattern matching will not match, as the key on the left
    # hand side is not in the map on the right hand side.
    # %{:c => c} = %{:a => 1, 2 => :b}
  end

  def adding_to_a_map do
    my_map = %{a: 1, b: 2, c: 3}
    my_map = Map.put(my_map, :d, 4)
    IO.puts Map.get my_map, :d
  end

  def updating_a_key_in_a_map do
    my_map = %{a: 1, b: 2, c: 4}
    IO.puts "now my_map key :c has value #{my_map[:c]}"
    my_map = %{my_map | :c => 3}
    IO.puts "now my_map key :c has value #{my_map[:c]}"
  end
end


Main.main
Main.adding_to_a_map
Main.updating_a_key_in_a_map
