# require makes it so that you need to write `Integer.` before
# functions of the module to use them. `import` makes the functions
# available without requiring you to write the imported module's name.
require Integer
# import Integer


defmodule Main do
  def main do
    Integer.is_odd(3)
  end
end

IO.inspect Main.main
