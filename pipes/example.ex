defmodule PipeTest do
  def square(x), do: x * x

  def sum(lst, start \\ 0) do
    start + Enum.sum(lst)
  end

  def sst(lst) do
    lst
    |> tl
    # An inspect step in between is always possible and simply shows
    # the in between result.
    |> IO.inspect
    # The first argument is the one which is the result of the
    # previous step in the pipe.
    |> sum(1)
    |> IO.inspect
    |> square
  end
end


IO.inspect PipeTest.sst([1,2,3])
