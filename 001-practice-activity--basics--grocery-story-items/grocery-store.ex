defmodule Main do
  def main do
    grocery = %{
      milk: 2,
      butter: 1,
      cocoa: 1
    }
    Map.put(grocery, :icecream, 6)
    Map.delete(grocery, milk)
  end
end


Main.main
