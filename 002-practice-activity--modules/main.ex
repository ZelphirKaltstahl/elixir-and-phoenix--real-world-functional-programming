defmodule Main do
  def main do
    grocery = %{
      milk: 2,
      butter: 1,
      cocoa: 1
    }
    Map.put(grocery, :icecream, 6)
    Map.delete(grocery, :milk)
  end

  def insert_grocery_item(map, {key, value}) do
    Map.put(map, key, value)
  end

  def remove_grocery_item(map, key) do
    Map.delete(map, key)
  end

  # def insert_grocery_item(map, item) do
  #   case item do
  #     {key, value} ->
  #       Map.put(map, key, value)
  #     _ -> {:error, "did not get a map item (tuple) as argument"}
  #   end
  # end
end

a = %{}
a = Main.insert_grocery_item(a, {:a, 1})
a = Main.insert_grocery_item(a, {:b, 2})
a = Main.remove_grocery_item(a, :a)
IO.inspect a
